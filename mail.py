#!/usr/bin/env python3
import argparse
import configparser
import getpass
import ipaddress
import json
import logging
import os
import subprocess
import sys

import jinja2

import re2oapi


path = os.path.dirname(os.path.abspath(__file__))

logger = logging.getLogger('mail')
handler = logging.StreamHandler(sys.stderr)
formatter = logging.Formatter('%(asctime)s - %(name)s[%(process)d] - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

if __name__ == "__main__":
	parser = argparse.ArgumentParser(
		description="Generate POSTIFX aliases and virtual files",
	)
	parser.add_argument("-e", "--export", help="Exporte le contenu du fichier d'alias puis virtual sur la sortie standard", action="store_true")
	parser.add_argument("-p", "--re2o-password", help="Demande le mot de passe de l'utilisateur re2o", action="store_true")
	parser.add_argument("-q", "--quiet", help="Diminue la verbosité des logs (à spécifier plusieurs fois pour diminuer la verbosité)", action='count', default=0)
	parser.add_argument("-r", "--re2o-server", help="Nom du serveur re2o à contacter", type=str, default=None)
	parser.add_argument("-u", "--re2o-user", help="Utilisateur re2o", type=str, default=None)
	parser.add_argument("-d", "--do-not-regen", help="Do not regen the mail server", action='store_true')
	parser.add_argument("-v", "--verbose", help="Augmente la verbosité des logs (à spécifier plusieurs fois pour augmenter la verbosité)", action='count', default=0)
	args = parser.parse_args()

	verbosity = args.verbose - args.quiet

	if verbosity <= -1:
		logger.setLevel(logging.WARNING)
	elif verbosity == 0:
		logger.setLevel(logging.INFO)
	elif verbosity >= 1:
		logger.setLevel(logging.DEBUG)

	logger.info("Reading configuration")
	with open(os.path.join(path, "mail.json")) as config_file:
		config = json.load(config_file)
	logger.debug("Loaded {}".format(config))

	logger.info("Reading Re2o configuration")
	re2o_config = configparser.ConfigParser()
	re2o_config.read(os.path.join(path, 're2o-config.ini'))

	if args.re2o_server is not None:
		api_hostname = args.re2o_server
	else:
		api_hostname = re2o_config.get('Re2o', 'hostname')
	if args.re2o_user is not None:
		api_username = args.re2o_user
	else:
		api_username = re2o_config.get('Re2o', 'username')
	if args.re2o_password:
		api_password = getpass.getpass('Re2o password: ')
	else:
		api_password = re2o_config.get('Re2o', 'password')

	logger.info("Connecting to Re2o")
	api_client = re2oapi.Re2oAPIClient(api_hostname, api_username, api_password, use_tls=False)

	logger.info("Querying Re2o")
	users = api_client.list("localemail/users")

	aliases_map = [
		{
			'alias' : alias['local_part'],
			'username' : alias['user'],
		} for user in users
		if not user['local_email_redirect']
		for alias in user['email_address'] ]

	virtual_map = [
		{
			'local_email' : alias['complete_email_address'],
			'redirection' : user['email'],
		} for user in users
		if user['local_email_redirect']
		for alias in user['email_address'] ]

	loginmap_map = [
		{
			'alias' : alias['local_part'],
			'username' : alias['user'],
		} for user in users
		for alias in user['email_address'] ]

	with open(os.path.join(path, 'templates', 'aliases.j2')) as aliases_template:
		aliases_template = jinja2.Template(aliases_template.read())

	with open(os.path.join(path, 'templates', 'virtual.j2')) as virtual_template:
		virtual_template = jinja2.Template(virtual_template.read())

	with open(os.path.join(path, 'templates', 'loginmap.j2')) as loginmap_template:
		loginmap_template = jinja2.Template(loginmap_template.read())

	if args.export:
		print("#################################")
		print("############# VIRTUAL ###########")
		print("#################################")
		with open(os.path.join(path, 'local', 'virtual_local')) as virtual_local:
			for line in virtual_local:
				print(line, end='')
		print(virtual_template.render(virtual_map=virtual_map))
		print("#################################")
		print("############# ALIASES ###########")
		print("#################################")
		with open(os.path.join(path, 'local', 'aliases_local')) as aliases_local:
			for line in aliases_local:
				print(line, end='')
		print(aliases_template.render(aliases_map=aliases_map))
		print("#################################")
		print("############# LOGINMAP  #########")
		print("#################################")
		with open(os.path.join(path, 'local', 'loginmap_local')) as loginmap_local:
			for line in loginmap_local:
				print(line, end='')
		print(loginmap_template.render(loginmap_map=loginmap_map))

	else:
		with open(os.path.join(path, 'generated', 'virtual'), 'w') as virtual_generated:
			with open(os.path.join(path, 'local', 'virtual_local')) as virtual_local:
				for line in virtual_local:
					virtual_generated.write(line)
			virtual_generated.write(virtual_template.render(virtual_map=virtual_map))
		with open(os.path.join(path, 'generated', 'aliases'), 'w') as aliases_generated:
			with open(os.path.join(path, 'local', 'aliases_local')) as aliases_local:
				for line in aliases_local:
					aliases_generated.write(line)
			aliases_generated.write(aliases_template.render(aliases_map=aliases_map))
		with open(os.path.join(path, 'generated', 'loginmap'), 'w') as loginmap_generated:
			with open(os.path.join(path, 'local', 'loginmap_local')) as loginmap_local:
				for line in loginmap_local:
					loginmap_generated.write(line)
			loginmap_generated.write(loginmap_template.render(loginmap_map=loginmap_map))


		if not args.do_not_regen :
			subprocess.run(['/usr/bin/newaliases'], stdout=subprocess.DEVNULL)
			subprocess.run(['/usr/sbin/postmap', os.path.join(path, 'generated', 'virtual')], stdout=subprocess.DEVNULL)
			subprocess.run(['/usr/sbin/postmap', os.path.join(path, 'generated', 'loginmap')], stdout=subprocess.DEVNULL)
			subprocess.run(['/usr/sbin/postfix', 'reload'])
